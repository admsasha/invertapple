#include "window.h"

#include <QtGui>
#include <QLabel>
#include <QPicture>
#include <QBitmap>
#include <QApplication>
#include <QRect>
#include <QDesktopWidget>
#include <QToolBar>
#include <QMessageBox>
#include <QStandardPaths>


window::window(){
    QRect desk(QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this)));
    move((desk.width() - width()) / 2, (desk.height() - height()) / 2);
    this->setFixedSize(295,335);

    QDir dirUserAppData(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (dirUserAppData.exists()==false) dirUserAppData.mkpath(dirUserAppData.path());

    this->setWindowIcon(QIcon(QString(PATH_USERDATA)+"/images/invertapple.png"));

    // background
    QLabel *fon = new QLabel(this);
    fon->setPixmap(QPixmap(QString(PATH_USERDATA)+"/images/fon.png"));
    fon->setGeometry(0,30,295,280);


    isGameRun=0;
    gameLevel=2;
    gameTurns=0;
    gameTimes=0;
    gameScore=0;

    // Расположение ходов
    lblTurn = new QLabel(this);
    lblTurn->move(20,307);

    // Расположение счетчика времени
    lblTimes = new QLabel(this);
    lblTimes->move(100,307);

    // Расположение счетчика времени
    lblScore = new QLabel(this);
    lblScore->move(200,307);


    signalMapper = new QSignalMapper(this);
    signalMapperTransfer = new QSignalMapper(this);


    int x=0, y=0;

    for (int i=1;i<26;i++){
        apple[i] = new Apple(this);

        apple[i]->move(10+x,30+y);
        apple[i]->setApple(1,0);

        x+=55;
        if (x>(4*55)){ x=0; y+=55; }

        connect(apple[i],SIGNAL(clicked()),signalMapper,SLOT(map()));
        connect(apple[i],SIGNAL(signalsEndTransfor()),signalMapperTransfer,SLOT(map()));
        signalMapper -> setMapping ( apple[i], i );
        signalMapperTransfer -> setMapping ( apple[i], i );
    }
    connect (signalMapper, SIGNAL ( mapped ( int ) ), this, SLOT ( apple_clicked ( int ) ) );
    connect (signalMapperTransfer, SIGNAL ( mapped ( int ) ), this, SLOT ( apple_endTransfer ( int ) ) );


    gameTimer = new QTimer();
    gameTimer->setInterval(1000);
    gameTimer->start();
    connect(gameTimer,SIGNAL(timeout()),this,SLOT(gameTimer_timeout()));


    setupToolBar();

    refreshScore();

}


void window::setupToolBar(){
    QToolBar *toolBar;
    QAction *newGame;
    QAction *setup;
    QAction *records;
    QAction *about;

    toolBar = new QToolBar("toolbar");
    toolBar->setMovable(0);
    addToolBar(Qt::TopToolBarArea, toolBar);

    newGame = new QAction( tr("New game"), this);
    setup = new QAction( tr("Setup"), this);
    records = new QAction( tr("Records"), this);
    about = new QAction( tr("About"), this);

    toolBar->addAction(newGame);
    toolBar->addAction(setup);
    toolBar->addAction(records);
    toolBar->addAction(about);


    connect(newGame, SIGNAL(triggered()), this, SLOT(newGame()));
    connect(setup, SIGNAL(triggered()), this, SLOT(showFrmSetup()));
    connect(records, SIGNAL(triggered()), this, SLOT(showFrmRecords()));
    connect(about, SIGNAL(triggered()), this, SLOT(about()));

}


void window::apple_endTransfer(int index){
    if (isGameRun!=1) return;
    if (isWin()==1){
        isGameRun=0;
        if (record::isNewRecord(gameLevel,gameTurns,gameTimes)){
            QMessageBox::information(this, tr("You win"), tr("Do you have a new record !"));
            query.exec();
            QString name=query.getName();
            record::setRecord(gameLevel,name,gameTurns,gameTimes);
        }else{
            QMessageBox::information(this, tr("You win"), tr("You win !"));
        }
    }
}

void window::apple_clicked(int index){
    if (isGameRun!=1){
        QMessageBox::information(this, tr("InvertApple"), tr("First you need to start a new game !"));
        return;
    }

    int row = (floor(((double)index/5.0)-0.1))*5.0+1.0;

    for (int i=0;i<5;i++){ apple[row+i]->startTransfor(); }

    for (int i=1;i<6;i++){
        if (index+(i*5)<=25) apple[index+(i*5)]->startTransfor();
        if (index-(i*5)>=1) apple[index-(i*5)]->startTransfor();
    }

    gameTurns++;
    refreshScore();
}

void window::newGame(){
    int nColor=0;

    for (int i=1;i<26;i++){
        apple[i]->setApple(1,0);
    }


    for (int x=0;x<gameLevel;x++){
        int index= int(1+qrand()%23);

        int row = (floor(((double)index/5.0)-0.1))*5.0+1.0;

        for (int i=0;i<5;i++){
            if (apple[row+i]->getNColor()==1)  nColor=2; else nColor=1;
            apple[row+i]->setApple(nColor,0);
        }

        for (int i=1;i<6;i++){
            if (index+(i*5)<=25){
                if (apple[index+(i*5)]->getNColor()==1)  nColor=2; else nColor=1;
                apple[index+(i*5)]->setApple(nColor,0);
            }
            if (index-(i*5)>=1){
                if (apple[index-(i*5)]->getNColor()==1)  nColor=2; else nColor=1;
                apple[index-(i*5)]->setApple(nColor,0);
            }
        }



    }


    gameTurns=0;
    gameTimes=0;
    isGameRun=1;
    refreshScore();
}

void window::about(){

    QString text=
        "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>"
        "1. COPYRIGHT, DISCLAIMER <BR>"
        "  (C) 1996-2011 AdmSasha <dik@inbox.ru> <BR>"
        "  This program is distributed in the hope that it will be useful,"
        "  but WITHOUT ANY WARRANTY; without even the implied warranty of "
        "  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<BR>"
        "<BR>"

        "2. CONTACT<BR>"
        "  Please send bugreports to: <a href=mailto://dik@inbox.ru>dik@inbox.ru</a><BR>"
        "  Only bug reports with a reproducable bug are accepted.<BR>"
        "<BR>"

        "3. LICENSE<BR>"
        "  The program is distributed under the GPLv3<BR>"
        "<BR>"

        "4. HISTORY<BR>"
        "   Version 1.0.0   27.10.2011<BR>"
        "   Version 1.1.0   17.07.2019<BR>"
        "";



    QMessageBox::information(this, tr("InvertApple"), text);
}


int window::isWin(){
    for (int i=1;i<26;i++){
        if (apple[i]->getNColor()==2) return 0;
    }

    return 1;
}

void window::gameTimer_timeout(){
    if (isGameRun==1) gameTimes++;
    refreshScore();
}


void window::refreshScore(){
    gameScore=gameLevel*50-(gameTurns*10)-(gameTimes);

    lblTurn->setText(tr("Turns")+": "+QString::number(gameTurns));
    lblTimes->setText(tr("Times")+": "+QString::number(gameTimes));
    lblScore->setText(tr("Score")+": "+QString::number(gameScore));
}

void window::showFrmRecords(){
    frmRecords r(this,gameLevel);
    r.exec();
    r.deleteLater();
}

void window::showFrmSetup(){
    frmSetup r(this,gameLevel);
    r.exec();
    gameLevel=r.getLevel();
    r.deleteLater();
}



