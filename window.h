#ifndef WINDOW_H
#define WINDOW_H

#include "Apple.h"

#include <QWidget>
#include <QMainWindow>
#include <QSignalMapper>
#include <QTimer>
#include <math.h>
#include "record.h"
#include "queryName.h"
#include "frmRecords.h"
#include "frmSetup.h"

class window : public QMainWindow {
    Q_OBJECT
    public:
        window();
    private:
        void setupToolBar();
        int isWin();
        int isGameRun;
        int gameTurns;
        int gameTimes;
        int gameLevel;
        int gameScore;
        int gameMaxScore;

        Apple *apple[26];
        QSignalMapper *signalMapper;
        QSignalMapper *signalMapperTransfer;
        QLabel *lblTurn;
        QLabel *lblTimes;
        QLabel *lblScore;
        QTimer *gameTimer;

        queryName query;


    private slots:
        void apple_clicked(int index);
        void apple_endTransfer(int index);
        void newGame();
        void showFrmSetup();
        void about();
        void gameTimer_timeout();
        void refreshScore();
        void showFrmRecords();

};

#endif // WINDOW_H
