#include "queryName.h"

queryName::queryName(QWidget *parent) :    QDialog(parent){
    this->setFixedSize(271, 96);
    this->setWindowTitle(tr("InvertApple"));
    this->setWindowIcon(QIcon(QString(PATH_USERDATA)+"/images/invertapple.png"));

    lineEdit = new QLineEdit(this);
    lineEdit->setGeometry(QRect(10, 30, 251, 25));

    label = new QLabel(this);
    label->setGeometry(QRect(10, 10, 251, 17));
    label->setAlignment(Qt::AlignCenter);
    label->setText(tr("Enter your name"));

    pushButton = new QPushButton(this);
    pushButton->setGeometry(QRect(90, 60, 88, 27));
    pushButton->setText(tr("OK"));

    connect(pushButton,SIGNAL(clicked()),this,SLOT(hide()));
}


QString queryName::getName(){
    return lineEdit->text();
}
