#include "record.h"

#include <QStandardPaths>
#include <QDebug>
#include <QFile>
#include <QDomDocument>


tRecord record::getRecord(int level){
    QString errMsg;
    int errLine;
    int ErrCol;

    tRecord tableRecord;
    sRecord r;


    QDomDocument xmlDoc("mydocument");
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/records.xml");
    if (!file.open(QIODevice::ReadOnly)) return tableRecord;
    if (!xmlDoc.setContent(&file,&errMsg,&errLine,&ErrCol)){
        qDebug() << "error pars  " << errMsg << errLine << ErrCol;
        file.close();
        return tableRecord;
    }
    file.close();


    QDomElement docElem = xmlDoc.documentElement();
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();

        if (e.tagName()=="level"+QString::number(level)){
            QDomNode n2 = e.firstChild();
            while(!n2.isNull()) {
                QDomElement e2 = n2.toElement();

                r.name= e2.attribute("name");
                r.times= e2.attribute("times").toInt();
                r.turns= e2.attribute("turns").toInt();
                r.score= e2.attribute("score").toInt();

                tableRecord.insert(e2.attribute("place").toInt(),r);

                n2  = n2.nextSibling();
            }
            break;
        }

        n  = n.nextSibling();
    }
    xmlDoc.clear();


    return tableRecord;
}

int record::isNewRecord(int level,int turns,int times){
    int score=level*50-(turns*10)-(times);
    int place=0;

    tRecord record = getRecord(level);

    // Вычисляем место
    for (int i=1;i<11;i++){
        if (record.contains(i)){
            if (score>record[i].score){
                place=i;
                break;
            }
        }else{
            record[i].score=score;
            place=i;
            break;
        }
    }

    if (place==0) return 0;

    return 1;
}

int record::setRecord(int level,QString name,int turns,int times){
    QString errMsg;
    int errLine;
    int ErrCol;

    int score=level*50-(turns*10)-(times);
    int place=0;

    tRecord record = getRecord(level);

    int countRecord = record.size();

    // Вычисляем место
    for (int i=1;i<11;i++){
        if (record.contains(i)){
            if (score>record[i].score){
                place=i;
                break;
            }
        }else{
            record[i].score=score;
            place=i;
            break;
        }
    }

    // Если место не занял, выходим
    if (place==0) return 0;


    // Делаем сдвиг
    for (int i=9;i>place-1;i--) record[i+1]=record[i];
    record[place].score=score;
    record[place].name=name;
    record[place].times=times;
    record[place].turns=turns;



    // Восоздаем новую структуру
    QDomDocument xmlDoc("mydocument");
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/records.xml");

    if (file.open(QIODevice::ReadOnly)){
        if (!xmlDoc.setContent(&file,&errMsg,&errLine,&ErrCol)){
            qDebug() << "error pars  " << errMsg << errLine << ErrCol;
            file.close();
            return 0;
        }
        file.close();
    }else{
        xmlDoc.setContent(QString("<records><level2/><level3/><level4/><level5/><level6/><level7/><level8/><level9/><level10/></records>"));
    }

    QDomElement elem = xmlDoc.createElement("record");
    elem.setAttribute("place", QString::number(countRecord+1));



    QDomElement docElem = xmlDoc.documentElement();
    QDomNode n = docElem.firstChild();
    while(!n.isNull()) {
        QDomElement e = n.toElement();

        if (e.tagName()=="level"+QString::number(level)){

            if (countRecord<10) e.appendChild(elem);

            QDomNode n2 = e.firstChild();
            while(!n2.isNull()) {
                QDomElement e2 = n2.toElement();

                e2.setAttribute("name",record.value(e2.attribute("place").toInt()).name);
                e2.setAttribute("score",QString::number(record.value(e2.attribute("place").toInt()).score));
                e2.setAttribute("times",QString::number(record.value(e2.attribute("place").toInt()).times));
                e2.setAttribute("turns",QString::number(record.value(e2.attribute("place").toInt()).turns));

                n2  = n2.nextSibling();
            }
            break;
        }

        n  = n.nextSibling();
    }




    // Сохранение
    if (!file.open(QIODevice::WriteOnly)) return 0;
    QTextStream out(&file);
    xmlDoc.save(out,0);
    file.close();


    xmlDoc.clear();



    return 1;
}

