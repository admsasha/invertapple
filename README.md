# README #
invertapple

Puzzle game in which you need to turn all red apples on the playing field to green.


![invertapple images](http://dansoft.krasnokamensk.ru/data/1015/invertapple_en.png)


### How do I get set up? ###
qmake

make

make install


### Who do I talk to? ###
email: dik@inbox.ru

www: https://dansoft.ru/
