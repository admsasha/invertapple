<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>frmRecords</name>
    <message>
        <location filename="frmRecords.cpp" line="7"/>
        <source>Close</source>
        <translation type="unfinished">Закрыть</translation>
    </message>
    <message>
        <location filename="frmRecords.cpp" line="21"/>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="frmRecords.cpp" line="25"/>
        <source>Score</source>
        <translation type="unfinished">Очков</translation>
    </message>
    <message>
        <location filename="frmRecords.cpp" line="37"/>
        <source>level</source>
        <translation type="unfinished">Уровень</translation>
    </message>
</context>
<context>
    <name>frmSetup</name>
    <message>
        <location filename="frmSetup.cpp" line="9"/>
        <source>complexity</source>
        <translation type="unfinished">Уровень</translation>
    </message>
    <message>
        <location filename="frmSetup.cpp" line="21"/>
        <source>Apply</source>
        <translation type="unfinished">Применить</translation>
    </message>
</context>
<context>
    <name>queryName</name>
    <message>
        <location filename="queryName.cpp" line="13"/>
        <source>Enter your name</source>
        <translation type="unfinished">Введите ваше имя</translation>
    </message>
    <message>
        <location filename="queryName.cpp" line="17"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>window</name>
    <message>
        <location filename="window.cpp" line="235"/>
        <source>Turns</source>
        <translation type="unfinished">Ходов</translation>
    </message>
    <message>
        <location filename="window.cpp" line="92"/>
        <source>New game</source>
        <translation type="unfinished">Новая игра</translation>
    </message>
    <message>
        <location filename="window.cpp" line="93"/>
        <source>Setup</source>
        <translation type="unfinished">Настройки</translation>
    </message>
    <message>
        <location filename="window.cpp" line="94"/>
        <source>Records</source>
        <translation type="unfinished">Рекорды</translation>
    </message>
    <message>
        <location filename="window.cpp" line="95"/>
        <source>About</source>
        <translation type="unfinished">О...</translation>
    </message>
    <message>
        <location filename="window.cpp" line="116"/>
        <location filename="window.cpp" line="121"/>
        <source>You win</source>
        <translation type="unfinished">Вы выиграли</translation>
    </message>
    <message>
        <location filename="window.cpp" line="116"/>
        <source>Do you have a new record !</source>
        <translation type="unfinished">У вас новый рекорд !</translation>
    </message>
    <message>
        <location filename="window.cpp" line="121"/>
        <source>You win !</source>
        <translation type="unfinished">Вы выиграли !</translation>
    </message>
    <message>
        <location filename="window.cpp" line="128"/>
        <location filename="window.cpp" line="214"/>
        <source>InvertApple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="window.cpp" line="128"/>
        <source>First you need to start a new game !</source>
        <translation type="unfinished">Сперва нажмите Новая игра</translation>
    </message>
    <message>
        <location filename="window.cpp" line="236"/>
        <source>Times</source>
        <translation type="unfinished">Время</translation>
    </message>
    <message>
        <location filename="window.cpp" line="237"/>
        <source>Score</source>
        <translation type="unfinished">Очки</translation>
    </message>
</context>
</TS>
