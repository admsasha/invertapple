#include "frmSetup.h"

frmSetup::frmSetup(QWidget *parent,int curLevel) :   QDialog(parent){
    this->setFixedSize(211, 94);
    this->setWindowTitle(tr("Setup"));
    this->setWindowIcon(QIcon(QString(PATH_USERDATA)+"/images/invertapple.png"));

    label = new QLabel(this);
    label->setGeometry(QRect(10, 16, 58, 21));
    label->setText(tr("complexity"));

    comboBox = new QComboBox(this);
    comboBox->setGeometry(QRect(80, 10, 121, 28));

    for (int i=2;i<11;i++){
        comboBox->addItem(QString::number(i));
    }
    comboBox->setCurrentIndex(curLevel-2);

    pushButton = new QPushButton(this);
    pushButton->setGeometry(QRect(60, 60, 88, 27));
    pushButton->setText(tr("Apply"));

    connect(pushButton,SIGNAL(clicked()),this,SLOT(hide()));

}

int frmSetup::getLevel(){
    return comboBox->currentText().toInt();
}
