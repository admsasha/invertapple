#ifndef FRMRECORDS_H
#define FRMRECORDS_H

#include <QDialog>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QTableWidget>
#include "record.h"

class frmRecords : public QDialog{
    Q_OBJECT

    public:
        explicit frmRecords(QWidget *parent = 0,int curLevel=0);

    private:

        QPushButton *pushButton;
        QTableWidget *tableWidget;
        QLabel *label;
        QComboBox *comboBox;

    signals:

    public slots:
        void refreshTable(int level);

};

#endif // FRMRECORDS_H
