#ifndef APPLE_H
#define APPLE_H

#include <QWidget>

#include <QWidget>
#include <QLabel>



class Apple : public QWidget {
Q_OBJECT
    public:
        Apple(QWidget *parent = nullptr);
        void setApple(int nColor,int nFrame);
        void startTransfor();
        int getNColor();
    private:
        QLabel *pic;
        QPixmap imgApple;

        int nColor,nFrame,endTransfor;
        int isTransfor;


    protected:
        bool eventFilter(QObject*, QEvent *);
	void timerEvent(QTimerEvent *event);

    signals:
        void clicked();
        void signalsEndTransfor();


};

#endif // APPLE_H
