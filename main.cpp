#include <QApplication>

#include <QtGlobal>
#include <QTime>
#include <QTextCodec>
#include <QTranslator>
#include <QCommandLineParser>

#include "window.h"


int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("DanSoft");
    QCoreApplication::setOrganizationDomain("dansoft.ru");
    QCoreApplication::setApplicationVersion("1.1.1");
    QCoreApplication::setApplicationName("Invert Apple");

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF8"));

    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));


    QCommandLineParser parser;
    parser.addVersionOption();
    parser.process(app);

    QTranslator translator;
    translator.load(QString(PATH_USERDATA)+QString("/langs/invertapple_") + QLocale::system().name());
    app.installTranslator(&translator);

    window Window;
    Window.show();
    return app.exec();
}
