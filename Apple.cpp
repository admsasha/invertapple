#include "Apple.h"

#include <QtGui>
#include <QBitmap>

Apple::Apple(QWidget *parent) : QWidget(parent){
    this->setFixedSize(55,55);
    pic = new QLabel(this);
    pic->setMouseTracking(true);
    pic->installEventFilter(this);
    pic->setGeometry(0,0,55,55);

    imgApple.load(QString(PATH_USERDATA)+"/images/apple.png");
    isTransfor=0;

}
bool Apple::eventFilter(QObject *o, QEvent *e){
    if (e->type() == QEvent::MouseButtonRelease){
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(e);
        if (mouseEvent->button() == Qt::LeftButton){
            if (o == pic){
                emit clicked();
                return 1;
            }
        }
    }

    return QWidget::eventFilter(o, e);
}


void Apple::setApple(int nColor,int nFrame) {
    QPixmap img;
    int x=0;
    int y=0;

    this->nColor=nColor;
    this->nFrame=nFrame;

    if (nColor==2) x=56;
    y=nFrame*56;

    img=imgApple.copy(x,y,55,55);
    pic->setPixmap(img);


}

int Apple::getNColor(){
    return nColor;
}

void Apple::startTransfor(){
    if (isTransfor==1) return;

    if (nColor==1) endTransfor=2; else endTransfor=1;
    nFrame=0;
    isTransfor=1;
    this->startTimer(20);
}


void Apple::timerEvent(QTimerEvent *event){
    int _nColor;
    int _nFrame;

    _nColor=nColor;
    _nFrame=nFrame;

    if (endTransfor!=nColor) _nFrame++; else _nFrame--;

    if (_nFrame>6){
        _nFrame=6;
        _nColor=endTransfor;
    }

    setApple(_nColor, _nFrame);

    if (endTransfor==nColor and nFrame==0){
        killTimer(event->timerId());
        isTransfor=0;
        emit signalsEndTransfor();
    }
}




