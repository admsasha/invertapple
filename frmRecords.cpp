#include "frmRecords.h"

frmRecords::frmRecords(QWidget *parent,int curLevel) :    QDialog(parent){
    this->setFixedSize(272, 422);
    this->setWindowTitle(tr("Records"));
    this->setWindowIcon(QIcon(QString(PATH_USERDATA)+"/images/invertapple.png"));

    pushButton = new QPushButton(this);
    pushButton->setText(tr("Close"));
    pushButton->setGeometry(QRect(180, 390, 88, 27));

    tableWidget = new QTableWidget(this);
    tableWidget->setColumnCount(2);
    tableWidget->setRowCount(10);
    tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);


    QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
    __qtablewidgetitem1->setText(tr("Name"));
    tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem1);

    QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
    __qtablewidgetitem2->setText(tr("Score"));
    tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem2);



    tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableWidget->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    tableWidget->setGeometry(QRect(0, 30, 271, 351));

    label = new QLabel(this);
    label->setText(tr("level"));
    label->setGeometry(QRect(55, 0, 79, 31));
    label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

    comboBox = new QComboBox(this);
    comboBox->setGeometry(QRect(140, 0, 131, 28));
    for (int i=2;i<11;i++){
        comboBox->addItem(QString::number(i));
    }
    comboBox->setCurrentIndex(curLevel-2);

    connect(pushButton,SIGNAL(clicked()),this,SLOT(hide()));
    connect(comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(refreshTable(int)));

    refreshTable(2);
}

void frmRecords::refreshTable(int level){

    tRecord r = record::getRecord(comboBox->currentText().toInt());

    tableWidget->setRowCount(0);
    tableWidget->setRowCount(10);

    for (int i=0;i<10;i++){

        if (!tableWidget->item(i,0)){
            QTableWidgetItem *newItem = new QTableWidgetItem();
            tableWidget->setItem(i,0,newItem);
        }
        if (!tableWidget->item(i,1)){
            QTableWidgetItem *newItem = new QTableWidgetItem();
            tableWidget->setItem(i,1,newItem);
        }


        if (r.contains(i+1)){
            tableWidget->item(i,0)->setText(r[i+1].name);
            tableWidget->item(i,1)->setText(QString::number(r[i+1].score));
        }
    }


}

