#ifndef QUERYNAME_H
#define QUERYNAME_H

#include <QDialog>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>

class queryName : public QDialog {
    Q_OBJECT

    public:
        explicit queryName(QWidget *parent = 0);
        QString getName();

    private:
        QLineEdit *lineEdit;
        QLabel *label;
        QPushButton *pushButton;

    signals:

    public slots:

};

#endif // QUERYNAME_H
