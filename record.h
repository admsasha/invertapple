#ifndef RECORD_H
#define RECORD_H

#include <QWidget>

struct sRecord {
    QString name;
    int turns;
    int times;
    int score;
};

typedef QMap<int,sRecord> tRecord;

class record : public QWidget{
    Q_OBJECT
    public:
        static tRecord getRecord(int level);
        static int setRecord(int level,QString name,int turns,int times);
        static int isNewRecord(int level,int turns,int times);

    signals:

    public slots:

};

#endif // RECORD_H
