#ifndef FRMSETUP_H
#define FRMSETUP_H

#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QComboBox>
#include <QDialog>
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>

class frmSetup : public QDialog {
    Q_OBJECT
    public:
        explicit frmSetup(QWidget *parent = 0,int curLevel=0);
        int getLevel();

    private:
        QLabel *label;
        QComboBox *comboBox;
        QPushButton *pushButton;


    signals:


    public slots:

};

#endif // FRMSETUP_H
